# Full-stack Agnostic Template

## Purpose:

The main purpose of this template was to create full-stack typesctipt template, 
which can be render or server framework agnostic.

## Features:

* Unidirectional Data Flow (with Redux)
* Code generation (for redux now)
* Truly testability (with separated App code base)
* TypeScript
* Node.js as platform for backend

## Pre-requisites (global):

* Node.js LTS
* TS 3.3.+
* gulp-cli
* parcel-bundler

## How-to:

* Dev run BE -> from backend/ -> npm start
* Dev run FE -> from frontend/ -> npm start

## Plans:

* [x] Go through code and clean all weird things.
* [ ] Sync the healthCheck feature folder structure with sample1.
* [ ] Add build stage into ci pipelines.
* [ ] Add production containers for frontend & backend.
* [x] Add code generation for type-constructors.

## Many thanks to:

**Vladimir Repin** (https://gitlab.com/vlr, vladimir.repin@emergn.com)
Actiually this template is his idea - evolution of his previous idea.
Many thanks for his help and review.

**Sergey Gerasimov** (sergey.gerasimov@emergn.com)
Thanks for review and good comments.

**Artur Taranchiev** (artur.taranchiev@emergn.com)
Thanks for good conversation related with CI and Docker.
