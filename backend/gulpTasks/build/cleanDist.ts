import * as fse from "fs-extra";

export async function cleanDist(): Promise<void> {
    const path = "dist/";
    const exists = await fse.pathExists(path);
    if (exists) {
      await fse.remove(path);
    }
}
