import { series } from "gulp";
import { buildTs } from "./buildTs";
import { cleanDist } from "./cleanDist";

export const buildBe = series(cleanDist, buildTs);
