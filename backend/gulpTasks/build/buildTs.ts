import { spawn } from "../tools/spawn";

export function buildTs() {
    return spawn("tsc", "tsc", "-p", "tsconfig.json");
}
