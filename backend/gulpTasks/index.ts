import { buildBe } from "./build/buildBe";
import { buildTs } from "./build/buildTs";
import { cleanDist } from "./build/cleanDist";
import { serve } from "./serve";
import { start } from "./start";

export {
    buildBe,
    buildTs,
    cleanDist,
    serve,
    start,
};
