import * as express from "express";
import { devMethods } from "./dev.methods";

const router = express.Router();
devMethods(router);
export const devRouter = router;
