Hey, **Contributor**!<br>
If u wanna contritbute into this repo, you should do several steps below:
* firstly, u should create a branch with rules below (**Ruleset #1**);
* further, u should write ur code and run tests locally;
* after successfully passed tests u should create merge request accordilng rules below (**Ruleset #2**);
* ...
* profit!


    
**Ruleset #1**:<br>
If you wanna fix the defect, you should create branch with following name:<br>
* bugfix/name_of_feature_short_defect_description OR
* bugfix/number_of_task_feature_short_defect_description OR
* bugfix/number_of_task_name_of_feature

**Ruleset #2**:<br>
If u wanna submit the pull request aka merge request, you should do following:<br>
* create short name for pull request similar with source branch name;
* create short, meaningfull description about the purpose of this PR/MR;
* check SQUASH commits and DELETE SOURCE BRANCH checkboxes;
* submit =)