import { Sample1Service } from "../beServices/sample1.service";
import { ItemId, sample1Item, Sample1Model, sample1Model } from "../contract";

export class Sample1Api {
  constructor(private service: Sample1Service) {
  }

  public async getModel(id: ItemId): Promise<Sample1Model> {
    const item = sample1Item(10);
    return sample1Model("name", item);
  }
}
