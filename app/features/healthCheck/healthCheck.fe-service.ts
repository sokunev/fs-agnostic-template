import { IFEContext, IWebApi } from "../../feContext";
import { HealthCheckStateDispatcher } from "./gen/health-check-state.dispatcher";
import { IHealthCheck } from "./healthCheck.type";

type IGetHealthCheckStatus = () => Promise<void>;

interface IHealthCheckFEService {
    getHealthCheckStatus: IGetHealthCheckStatus;
}

export class HealthCheckFEService implements IHealthCheckFEService {
    constructor(
        private dispatcher: HealthCheckStateDispatcher,
        private http: IWebApi) { }

    public async getHealthCheckStatus(): Promise<void> {
        try {
            const model = await this.http.get<IHealthCheck>("http://localhost:3000/dev/healthCheck");
            this.dispatcher.updateHealthCheckStatus({...model});
        } catch (e) {
            // tslint:disable-next-line: no-console
            console.error(e);
        }
    }
}

export function healthCheckFEService(context: IFEContext): HealthCheckFEService {
    const dispatcher = new HealthCheckStateDispatcher(context.store);
    const http = context.webApi;
    return new HealthCheckFEService(dispatcher, http);
}
