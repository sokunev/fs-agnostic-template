import { healthCheckBEService } from "./healthCheck.be-service";
import { IHealthCheck } from ".";

describe("Health check sample test", () => {
  it("should return OKAY status", () => {
    const res = healthCheckBEService().getAppStatus();
    const expected: IHealthCheck = {
      isRunning: true,
      statusText: "Everything is okay!",
    };

    expect(res).toMatchObject(expected);
  });
});
