import { IHealthCheck } from "./healthCheck.type";

export class HealthCheckBEService {
    public getAppStatus(): IHealthCheck {
        return {
            isRunning: true,
            statusText: "Everything is okay!",
        };
    }
}

export const healthCheckBEService = (): HealthCheckBEService => {
    return new HealthCheckBEService();
};
