export interface IHealthCheck {
    isRunning: boolean;
    statusText: string;
}
