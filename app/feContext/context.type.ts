import { IStoreService, IWebApi } from "./";

export interface IFEContext {
    store: IStoreService;
    webApi: IWebApi;
}
