export type IHttpTool = <T>(request: RequestInfo) => Promise<T>;

export async function http<T>(request: RequestInfo): Promise<T> {
    try {
        const result = await fetch(request);
        return await result.json();
    } catch (err) {
        // tslint:disable-next-line: no-console
        console.error(err);
        throw new Error(err);
    }
}
