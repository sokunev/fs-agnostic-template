import { IWebApi } from "../../../app/feContext";
import { IHttpTool } from "./http.service";

export class WebApiService implements IWebApi {
    constructor(private webApi: IHttpTool) { }

    public async get<T>(
        url: string,
        params: RequestInit = { method: "get" },
    ): Promise<T> {
        return await this.webApi<T>(new Request(url, params));
    }

    public async put<T>(
        url: string,
        body: any,
        params: RequestInit = { method: "put", body: JSON.stringify(body) },
    ): Promise<T> {
        return await this.webApi<T>(new Request(url, params));
    }

    public async post<T>(
        url: string,
        body: any,
        params: RequestInit = { method: "post", body: JSON.stringify(body) },
    ): Promise<T> {
        return await this.webApi<T>(new Request(url, params));
    }

    public async delete<T>(
        url: string,
        params: RequestInit = { method: "delete" },
    ): Promise<T> {
        return await this.webApi<T>(new Request(url, params));
    }
}
