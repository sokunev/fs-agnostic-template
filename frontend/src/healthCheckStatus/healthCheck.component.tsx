import * as React from "react";
import { healthCheckFEService, HealthCheckFEService } from "../../../app/features/healthCheck/healthCheck.fe-service";
import { HealthCheckState } from "../../../app/features/healthCheck/healthCheck.state";

import { feContext } from "../feContext.service";

export class HealthCheck extends React.Component<{}, HealthCheckState> {
    private healthCheckService: HealthCheckFEService;

    constructor(props: any) {
        super(props);

        this.state = {
            isRunning: false,
            statusText: "need to check",
        };

        this.healthCheckService = healthCheckFEService(feContext);
    }

    public componentDidMount() {
        feContext.store.subscribe((state) => this.setState(state.healthCheck));
    }

    public render() {
        return (
            <div>
                <button onClick={async () => await this.getHealthCheckStatus()}>
                    {"Health check"}
                </button>
                <h2>{"status"}</h2>
                <p>{"Here is the status"}</p>
                <p>{this.state.isRunning}</p>
                <p>{this.isRunningString(this.state.isRunning)}</p>
                <p>{this.state.statusText}</p>
            </div>
        );
    }

    private async getHealthCheckStatus(): Promise<void> {
        this.healthCheckService.getHealthCheckStatus();
    }

    private isRunningString(status: boolean): string {
        return status ? "Application is running with status below" : "Application is not running";
    }
}
