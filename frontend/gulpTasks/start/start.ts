import { series } from "gulp";
import { deRedux } from "../../../app/gulpTasks/automate/deRedux";
import { spawn } from "../tools/spawn";

export function startDev() {
    return spawn("npm", "npm", "run", "build:ts:dev");
}

export const runDev = series(deRedux, startDev);
